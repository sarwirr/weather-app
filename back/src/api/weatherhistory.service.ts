import { Injectable } from "@nestjs/common";
import axios from "axios";

@Injectable()
export class WeatherHistoryService {

    async fetchDataFromExternalApi(location: string): Promise<any> {
        const option ={
            method: 'GET',
            url: `https://api.tomorrow.io/v4/weather/forecast?location=${location}&timesteps=1d&apikey=${process.env.TOMORROW_API_KEY}`,
            headers : {
                accept: 'application/json'
            },
        };
        console.log(option.url);
    try {
        const response = await axios.request(option);
        return response.data;

    } catch (error) {
        throw new Error(`Error fetching data from the external API: ${error.message}`);
    }


    }
}