import { Model } from 'mongoose';


export class MongoGenericRepository<T> implements MongoGenericRepository<T> {
    private _repository: Model<T>;
    private _populateOnFind: string[];
  
    constructor(repository: Model<T>, populateOnFind: string[] = []) {
      this._repository = repository;
      this._populateOnFind = populateOnFind;
    }
  
    getAll(): Promise<T[]> {
      return this._repository.find().populate(this._populateOnFind).exec();
    }
  
    get(name: any): Promise<any> {
      return this._repository.findOne(name).populate(this._populateOnFind).exec();
    }
  
    create(item: T): Promise<T> {
      return this._repository.create(item);
    }
  
    update(id: any, item: T): Promise<T> { // Update the return type to Promise<T>
     
      return this._repository.findOneAndUpdate({id :id }, item);
    }
  }