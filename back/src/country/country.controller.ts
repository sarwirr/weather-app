import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Res, NotFoundException } from '@nestjs/common';
import { CountryService } from './country.service';
import { CreateCountryDto } from './dto/create-country.dto';
import { WeatherHistoryService } from 'src/api/weatherhistory.service';
import { UpdateCountryDto } from './dto/update-country.dto';
import mongoose from 'mongoose';

@Controller('country')
export class CountryController {
  constructor(private readonly countryService: CountryService,
    private readonly WeatherHistoryService: WeatherHistoryService ) {}


  
  @Post()
  create(@Body() createCountryDto: CreateCountryDto) {
    return ('This action adds a new country');
  }
 
  @Get('fetch-weather-history/:location')
  async fetchData(@Param('location') location: string): Promise<any> {
      const data = await this.WeatherHistoryService.fetchDataFromExternalApi(location);
      

      const temperatureData = data.timelines.daily.map((dailyData: any) => ({
        date: dailyData.time,
        temperature: dailyData.values.temperatureAvg,
      }));

      // Find the country by location or create a new one if it doesn't exist
      const country = await this.countryService.findOrCreateCountry(location , temperatureData); 
      
      
      const currentDate= new Date();
      // Format the current date to include only the year, month, and day (YYYY-MM-DD)
      const currentDateFormatted = currentDate.toISOString().slice(0, 10);
      console.log(currentDateFormatted);
      
      // Find the temperature data point that matches the date (ignoring time)
      const todayTemperatureData = country.temperatureData.find((data) => {
        const dataDateFormatted = data.date.toISOString().slice(0, 10);
        return dataDateFormatted === currentDateFormatted;
      });
      
      console.log(todayTemperatureData);
      // console.log("heeeere");
      if (todayTemperatureData) {
        
        const todayTemperature = todayTemperatureData.temperature;
        const todayCondition = setTemperatureCondition(todayTemperature);
        
        const CurrentWeather = {temperature: todayTemperature, condition: todayCondition};
         console.log(CurrentWeather);
        
        await this.countryService.update(location, {temperatureData: temperatureData, CurrentWeather: CurrentWeather});
        

} 
    return (country);  
  }


}
function setTemperatureCondition(temperature: Number) { // Use a custom setter function to set the condition
  let condition = 'normal';
  if (temperature.valueOf() > 30) {
    condition = 'high';
  } else if (temperature.valueOf() < 20) {
    condition = 'low';
  }

  return (condition);
}


