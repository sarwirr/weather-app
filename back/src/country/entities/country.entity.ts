
import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Date, Document ,Types} from 'mongoose';

export type CountryDocument = Country;

@Schema()
export class Country {

    @Prop
     ({unique: true
      })
    name: string;

    @Prop({ type: [{ date: Date, temperature: Number }] })
  temperatureData: Array<{ date: Date; temperature: Number }>;

  @Prop({  set: () => Date()})
  LastCalledApi: string;

  @Prop({ type: { temperature: Number, condition: String }}) 
  CurrentWeather: { temperature: Number; condition: string };
    }
    
function setTemperatureCondition(temperature: Number) { // Use a custom setter function to set the condition
  let condition = 'normal';
  if (temperature.valueOf() > 35) {
    condition = 'high';
  } else if (temperature.valueOf() < 20) {
    condition = 'low';
  }

  return { temperature, condition };
}

 const CountrySchema = SchemaFactory.createForClass(Country);

 export {CountrySchema}

