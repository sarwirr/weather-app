import { Module } from '@nestjs/common';
import { CountryService } from './country.service';
import { CountryController } from './country.controller';
import { WeatherHistoryService } from 'src/api/weatherhistory.service';
import { mongo } from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { CountrySchema } from './entities/country.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Country', schema: CountrySchema },
    ]),
  ],
  controllers: [CountryController],
  providers: [CountryService, WeatherHistoryService],
})
export class CountryModule {}
