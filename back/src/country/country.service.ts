import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Country, CountryDocument } from './entities/country.entity';
import { MongoGenericRepository } from 'src/repository/generic-repository';
import { CreateCountryDto } from './dto/create-country.dto';
import { UpdateCountryDto } from './dto/update-country.dto';

@Injectable()
export class CountryService {
  constructor(
    @InjectModel(Country.name) private countryModel: Model<CountryDocument>,
  ) {}

  private MongoGenericRepository = new MongoGenericRepository<CountryDocument>(
    this.countryModel,
  );

  async create(location: string , data :any) {
    

    const country= await this.MongoGenericRepository.create({
      name: location,
      temperatureData: data,
      LastCalledApi: Date(),
      CurrentWeather: { temperature: 0, condition: 'normal' },
    });
    
    return country;
  }

  async findOrCreateCountry(location: string , data:any) {
    const country = await this.MongoGenericRepository.get({ name: location });

      if (country) {
      return country;
    } else {
      return await this.create(location, data);
     
      
    }
  }

  findAll() {
    return `This action returns all country`;
  }

  async findOne(name: string) {
    const country = await this.MongoGenericRepository.get({ name: name });
    return country;
  }

  async update(name: any, updateCountryDto: UpdateCountryDto) {

    const country = await this.MongoGenericRepository.get({ name: name });
    
    const updatedCountry = {
      ...country,
      ...updateCountryDto,
    };
    return await this.MongoGenericRepository.update(name, updatedCountry);
  }

  remove(id: number) {
    return `This action removes a #${id} country`;
  }
}