import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CountryModule } from './country/country.module';
import { MongooseModule } from '@nestjs/mongoose';

require('dotenv').config(); 

@Module({
  imports: [CountryModule,
   MongooseModule.forRoot(
  process.env.MONGODB_KEY
  ,)
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
