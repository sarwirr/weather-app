import { QueryClientProvider } from '@tanstack/react-query'
import  { PropsWithChildren } from 'react'
import { queryClient } from '../lib/query'
import { WeatherConfigProvider } from '../context/weatherConfigContext'
import { ConfigProvider } from 'antd'
export default function Providers({children}:PropsWithChildren){
    return (
        <ConfigProvider>
        <QueryClientProvider client={queryClient}>
            <WeatherConfigProvider>
        {children}</WeatherConfigProvider>
        </QueryClientProvider></ConfigProvider>
    )
}