import { PropsWithChildren } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

export function OurCaroussel(props:PropsWithChildren){
return (<Carousel
  swipeable={false}
  draggable={false}
  showDots={true}
  responsive={responsive}
  
  infinite={true}
  autoPlay
  autoPlaySpeed={3000}
  keyBoardControl={true}
    sliderClass="h-full"
  containerClass="carousel-container h-fit p-20 w-3/5 mx-auto"
    dotListClass="custom-dot-list-style"
  itemClass="carousel-item-padding-40-px"
>
{props.children}
</Carousel>);
}