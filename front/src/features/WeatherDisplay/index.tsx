import React, { useState, useEffect } from 'react';
import { WeatherCarousel } from './cardCarousel';
import axios from 'axios';
import { useWeatherConfigGetter } from '../../context/weatherConfigContext';
import { Spin } from 'antd';
import { Chart } from './chart';


type TemperatureData = {
  date: string;
  temperature: number;
};

export type TransformedTemperatureData = {
  date: Date;
  temperature: number;
  condition: "cold"| "warm" | "hot";
};

type CurrentWeather = {
  temperature: number;
  condition: string;
  _id: string;
};

type Location = {
  _id: string;
  name: string;
  temperatureData: TemperatureData[];
  LastCalledApi: string;
  CurrentWeather: CurrentWeather;
  __v: number;
};



function toUnit(valueInC: number, unit: "C" | "F") {
  if (unit === "C") {
    return valueInC;
  } else {
    return (valueInC * 9) / 5 + 32;
  }
}


export function WeatherDisplay() {
  const { city,unit } = useWeatherConfigGetter();
  const [loading, setLoading] = useState(false);
  const [weatherData, setWeatherData] = useState<TemperatureData[]>([]);



  useEffect(() => {
    // Fetch weather data for the selected country
    setLoading(true);

    axios
      .get<Location>(`${process.env.REACT_APP_API_URL}/country/fetch-weather-history/${city}`)
      .then((response) => {
        const tempArray = response.data.temperatureData.map(
            (element) => ({
            date: element.date,
            temperature: element.temperature,
          }));
          console.log("tempArray",tempArray);
          console.log("success");
       
        setWeatherData(tempArray);
       // fill the weatherData array with dummy data
    
        setLoading(false);

      })
      .catch((error) => {
        console.log("error");
        console.error('Error fetching weather data:', error);
        setLoading(false);
      });
  }, [city]);
  
  const FinalWeatherData:TransformedTemperatureData[]=weatherData.map((element) => ({
    date: new Date(element.date),
    temperature: toUnit(element.temperature,unit),
    condition: element.temperature < 15 ? "cold" : element.temperature < 30 ? "warm" : "hot",
  }));


  return (
    <div className='w-full px-8 flex justify-center items-center flex-col'>
      <h2 className='text-center text-3xl font-black '>Weather Forecast for {city}</h2>
      <div className='w-full h-full'>{!loading ? (
        <><WeatherCarousel weatherData={FinalWeatherData} />
        <Chart weatherData={FinalWeatherData} /></>
      ) : (
        <Spin size="large" />
      )}</div>
      
    </div>
  );
}
