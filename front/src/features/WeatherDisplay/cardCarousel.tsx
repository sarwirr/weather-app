import { OurCaroussel } from './Caroussel';
import { TransformedTemperatureData } from './index';
import { useWeatherConfigGetter } from '../../context/weatherConfigContext';
import cold from '../../assets/cold.jpeg'
import warm from '../../assets/warm.jpeg'
import hot from '../../assets/hot.jpeg'
interface CardCarouselProps {
    weatherData:TransformedTemperatureData[];
    }

export function WeatherCarousel({ weatherData }: CardCarouselProps) {
  const  {city,unit}=useWeatherConfigGetter();
  
  return (
    
    <OurCaroussel>

      {
        weatherData.sort(
          (a,b)=>a.date>=b.date?1:-1
        ).map((element,index) => (
          <div
            key={element.date.toISOString()+city}
            className='outline outline-gray-600 outline-2 py-11 px-4 flex flex-col justify-center items-center gap-3 w-fit rounded-lg'
            style={{
              // a gradient that changes depending on the temperature
              backgroundImage: `url(${
                element.condition === 'cold' ? cold : element.condition === 'warm' ? warm : hot
              })`,
              backgroundSize: 'cover',
              outlineColor:
                element.date.getDate()===new Date().getDate()?'red':'gray'

             
              , outlineWidth: element.date.getDate()===new Date().getDate()?'5px':'2px'
            }}
          >
            <div className='text-white text-xl font-bold'>
              {element.date.getDate()}/{element.date.getMonth() + 1}/{element.date.getFullYear()}
            </div>
            <div className='text-white text-6xl font-extrabold'>{
              element.temperature.toPrecision(4)}
              <sup>{
                unit
                }</sup>
              </div>
          </div>
        ))
      }


    </OurCaroussel>
  );
}
