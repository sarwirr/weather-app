import { Column } from "@ant-design/plots";
import { TransformedTemperatureData } from ".";

interface ChartProps {
    weatherData:TransformedTemperatureData[];
}
export function Chart ({ weatherData:data }: ChartProps){
    const config :any = {
        data:
          data.map((element)=>({
            date: element.date.toISOString(),
            temperature: element.temperature,
            condition: element.condition,
          })),
        xField: 'date',
        yField: 'temperature',
        seriesField: '',
        
        
        legend: false,
        xAxis: {
          label: {
            autoHide: true,
            autoRotate: false,
          },
        },
      };
      return <Column {...config} />;

}