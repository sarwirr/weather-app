import React, { useState } from 'react';
import { Button } from 'antd';

export function UnitSelector({ selectedUnit, onUnitChange }: {
  selectedUnit: "C" | "F";
  onUnitChange: (value: "C" | "F") => void;
}) {
  // Use the useState hook to maintain the selected unit state
  const [internalSelectedUnit, setInternalSelectedUnit] = useState(selectedUnit);

  const handleUnitChange = (unit: "C" | "F") => {
    setInternalSelectedUnit(unit); // Update the internal state
    onUnitChange(unit); // Notify the parent component of the change
  };
  //console.log("internalSelectedUnit",internalSelectedUnit);

  return (
    <div className='flex flex-row gap-3 items-center' >
     <Button
     size={
      internalSelectedUnit === 'C' ? 'large' : 'middle'
     }
      type={internalSelectedUnit === 'C' ? 'primary' : 'default'}
      onClick={() => handleUnitChange('C')}
      style={{ backgroundColor: internalSelectedUnit === 'C' ? 'blue' : 'white' , fontWeight:"bolder" }}
>
        Celsius
      </Button>
      <Button
size={
  internalSelectedUnit === 'C' ? 'middle' : 'large'
 }        type={internalSelectedUnit === 'F' ? 'primary' : 'default'}
        onClick={() => handleUnitChange('F')}
        style={{ backgroundColor: internalSelectedUnit === 'F' ? 'blue' : 'white', fontWeight:"bolder"  }}
      >
        Fahrenheit
      </Button>
    </div>
  );
}
