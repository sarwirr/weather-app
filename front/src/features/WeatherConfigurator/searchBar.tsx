import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { AutoComplete } from 'antd';

export function SearchBar({ onSelect }: {
  onSelect: (value: string) => void;
}) {
  const [searchInput, setSearchInput] = useState('');
  const [suggestions, setSuggestions] = useState<string[]>([]);
  const [countries, setCountries] = useState<string[]>([]);
  const [selectedCountry, setSelectedCountry] = useState<string | null>();

  useEffect(() => {
    // Make an API request to fetch the list of countries
    axios
      .get('https://restcountries.com/v3.1/all')
      .then((response) => {
        // Extract country names from the API response
        const countryNames = response.data.map(
          (country: { name: { common: string } }) => 
          country.name.common
        );
        setCountries(countryNames);

      })
      .catch((error) => {
        console.error('Error fetching countries:', error);
      });
  }, []); // This effect runs once when the component mounts to fetch the country list

  useEffect(() => {
    // Use a timer to debounce the API request
    const debounceTimer = setTimeout(() => {
      // Filter the list of countries based on the user's input
      const filteredCountries = countries.filter((country) =>
        country.toLowerCase().startsWith(searchInput.toLowerCase())
      );

      setSuggestions(filteredCountries);
    }, 500); // Adjust the debounce time as needed

    // Cleanup the timer on component unmount or when the input changes
    return () => clearTimeout(debounceTimer);
  }, [searchInput, countries]);
  
  const options = suggestions.map((country) => ({
    value: country,
  }));

  // console.log("options",options);

  return (
    <AutoComplete
    style={{ width: 400 , }}
  size='large'
    onSearch={setSearchInput}
    placeholder="Search for a country"
    value={selectedCountry}
    onChange={(v) => setSelectedCountry(v)}
    onSelect={(value) => onSelect(value)} 
  >
    {options.map((option) => (
      <AutoComplete.Option key={option.value} value={option.value}>
        {option.value}
      </AutoComplete.Option>
    ))}
  </AutoComplete>
  );
}
