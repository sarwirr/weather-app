import { SearchBar } from "./searchBar";
import { UnitSelector } from "./unitSelector";
import { useState } from 'react';
import { useWeatherConfig } from "../../context/weatherConfigContext";

  
export function WeatherConfigurator() {
    const [, setSelectedCountry] = useState('');
   
    const {setCity,setUnit,unit}=useWeatherConfig();

    // const handleUnitChange = (unit: "C"|"F") => {
    //     setUnit(unit);
    //     // console.log('Selected Unit:', unit); // Log the selected unit
    //   };

      const handleSelectedCountry = (selectedCountry: string) => {
        setSelectedCountry(selectedCountry);
        setCity(selectedCountry); // Update the city in the context
        // console.log('Selected Country:', selectedCountry); // Log the selected country
      };
    
      return (
        <div className="w-full h-git px-9 py-11 flex flex-col items-center gap-5">
          <SearchBar onSelect={handleSelectedCountry} />
          <div>
            
            <UnitSelector
              onUnitChange={(v) => {
                setUnit(v);
              } } selectedUnit={unit}            />
            
          </div>
         </div>
      );
    }