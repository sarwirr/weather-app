
import './App.css';
import Providers from './Providers';
import { WeatherConfigurator } from './features/WeatherConfigurator';
import { WeatherDisplay } from './features/WeatherDisplay';

function App() {
  return (
    <Providers>
      <WeatherConfigurator/>
      <WeatherDisplay/>
    
      
    </Providers>
  );
}

export default App;
