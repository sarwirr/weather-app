import { PropsWithChildren, createContext, useContext, useState } from "react";

interface WeatherConfigType {
    unit: "C"| "F";
    city: string;
}

const WeatherConfigContext = createContext<WeatherConfigType &{
    setUnit: (unit: "C"| "F")=>void;
    setCity: (city: string)=>void;
}>({
    unit: "C",
    city: "Tunisia",
    setUnit: ()=>{},
    setCity: ()=>{}
});

export function useWeatherConfigGetter(){
    const {unit, city} = useContext(WeatherConfigContext);
    return {unit, city};
}

export function useWeatherConfigSetter(){
    const {setUnit, setCity} = useContext(WeatherConfigContext);
    return {setUnit, setCity};
}

export function useWeatherConfig(){
    return useContext(WeatherConfigContext);
}


export function WeatherConfigProvider({children}:PropsWithChildren){
    const [unit, setUnit] = useState<"C"| "F">("C");
    const [city, setCity] = useState<string>("Tunisia");
    return (
        
       
        <WeatherConfigContext.Provider value={{
            unit,
            city
        , setUnit, setCity
        }}>
            {children}
        </WeatherConfigContext.Provider>
    )
}

