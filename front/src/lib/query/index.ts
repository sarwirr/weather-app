import { DefaultOptions, QueryClient } from "@tanstack/react-query";


const queryConfig:DefaultOptions = {
    queries: {
        refetchOnWindowFocus: true,
        retry: false,
        staleTime: Infinity,
    },
    };

    export const queryClient = new QueryClient({
    defaultOptions: queryConfig,
    });